package br.com.cartao.cartao.repositories;

import br.com.cartao.cartao.models.Cartao;
import org.springframework.data.repository.CrudRepository;

import java.util.Optional;

public interface CartaoRepository extends CrudRepository<Cartao, Integer> {
    Optional<Cartao> findById(int id);

    Optional<Cartao> findByNumero(int numero);
}
