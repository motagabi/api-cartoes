package br.com.cartao.cartao.repositories;

import br.com.cartao.cartao.models.Cliente;
import org.springframework.data.repository.CrudRepository;
import java.util.Optional;

public interface ClienteRepository extends  CrudRepository<Cliente, Integer>{
    Optional<Cliente> findById(int id);


}
