package br.com.cartao.cartao.controllers;

import br.com.cartao.cartao.models.Cartao;
import br.com.cartao.cartao.services.CartaoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;

@RestController//transforma em classe controladora do spring
@RequestMapping("/cartao")
public class CartaoController {
    @Autowired
    private CartaoService cartaoService;

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Cartao cadastrarCartao(@RequestBody @Valid Cartao cartao){
        Cartao objetoCartao = cartaoService.salvarCartao(cartao);
        return objetoCartao;
    }

    @GetMapping("/{id}")
    public Cartao pesquisarPorId(@PathVariable(name = "id") int id){
        try{
            Cartao cartao= cartaoService.buscarCartaoPeloId(id);
            return cartao;
        }catch (RuntimeException e){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }
    }
    @PutMapping("/{numero}")
    public Cartao ativaCartao(@RequestBody @Valid Cartao cartao, @PathVariable(name="numero") int numero){
        try {
            Cartao cartaoDB= cartaoService.ativaCartao(numero , cartao);
            return cartaoDB;
        }catch (RuntimeException e){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }
    }
}
