package br.com.cartao.cartao.services;

import br.com.cartao.cartao.models.Cliente;
import br.com.cartao.cartao.repositories.ClienteRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class ClienteService {
    @Autowired
    private ClienteRepository clienteRepository;

    public Cliente salvarCliente(Cliente cliente){
        Cliente objetoCliente = clienteRepository.save(cliente);
        return objetoCliente;
    }

    public Cliente buscarClientePeloId(int id){
        Optional<Cliente> clienteOptional = clienteRepository.findById(id);
        if(clienteOptional.isPresent()){
            Cliente cliente = clienteOptional.get();
            return cliente;
        }else{
            throw new RuntimeException("O cliente "+ id +" não foi encontrado");
        }
    }
}
