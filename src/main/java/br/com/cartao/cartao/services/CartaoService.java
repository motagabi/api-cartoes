package br.com.cartao.cartao.services;

import br.com.cartao.cartao.models.Cartao;
import br.com.cartao.cartao.models.Cliente;
import br.com.cartao.cartao.repositories.CartaoRepository;
import br.com.cartao.cartao.repositories.ClienteRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class CartaoService {
    @Autowired
    private CartaoRepository cartaoRepository;

    @Autowired
    private ClienteRepository clienteRepository;

    public Cartao salvarCartao(Cartao cartao) {
        /*
        Optional<Cliente> clienteOptional = clienteRepository.findByClienteId(Cliente.class);

        if (clienteOptional.isPresent()) {
            Cliente cliente = clienteOptional.get();
            try {
                Cartao objetoCartao = new Cartao();
                cartaoRepository.save(objetoCartao);
                return objetoCartao;
            } catch (RuntimeException ex) {
                throw new RuntimeException("Erro ao salvar Cartão: " + ex.getMessage());
            }
        } else {
            throw new RuntimeException("Cliente id: (" + clienteOptional + ") inválido ou inexistente.");
        }
        */
        Cartao objetoCartao = cartaoRepository.save(cartao);
        return objetoCartao;
    }
    public Cartao buscarCartaoPeloId(int id){
        Optional<Cartao> cartaoOptional = cartaoRepository.findById(id);
        if(cartaoOptional.isPresent()){
            Cartao cartao = cartaoOptional.get();
            return cartao;
        }else{
            throw new RuntimeException("O cartão "+ id +" não foi encontrado");
        }
    }
    public Cartao buscarCartaoPeloNumero(int numero){
        Optional<Cartao> cartaoOptional = cartaoRepository.findByNumero(numero);
        if(cartaoOptional.isPresent()){
            Cartao cartao = cartaoOptional.get();
            return cartao;
        }else{
            throw new RuntimeException("O cartao "+ numero +" não foi encontrado");
        }
    }
    public Cartao ativaCartao(int numero, Cartao cartao){
        Cartao cartaoDB = buscarCartaoPeloNumero(numero);
        if(cartaoDB.isAtivo()){
            cartao.setAtivo(false);
        }
        cartao.setId(cartaoDB.getId());
        return cartaoRepository.save(cartao);
    }
}
